#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
import glob
import os
import shutil
import pathlib
from PIL import Image

"""
Need to have both s3 buckets pulled down before running this program
aws s3 cp --recursive s3://aidenoliver.com/uploads uploads
aws s3 cp --recursive s3://aidenoliver.com/images images
"""


def create_html():
    env = Environment(loader=FileSystemLoader("templates"))
    # grab baby image list from file
    with open("ls-s3-baby-files") as infile:
        baby_images = infile.readlines()
    ls_file_list = []
    for baby_image_line in baby_images:
        baby_image_list = baby_image_line.split(" ")
        file_name = baby_image_list[-1].replace("\n", "")
        if len(file_name) > 1:
            ls_file_list.append(f"images/baby/{file_name}")

    existing_image_list = [file for file in ls_file_list if ".jpg" in file]

    new_image_list = glob.glob("images/baby/*.jpg")
    image_description_list = glob.glob("images/baby/*.txt")

    full_image_list = existing_image_list + new_image_list

    image_dict = {}
    full_image_list.sort(reverse=True)

    for image in full_image_list:
        og_image_name = image.split("--")[1]
        image_desc_file = f"images/baby/{og_image_name.replace('jpg', 'txt')}"
        if image_desc_file in image_description_list:
            # if a matched filename ending in .txt exists then extract it's content
            # as an image description
            with open(image_desc_file) as infile:
                image_desc = infile.read().strip("\n")
            image_dict[image] = image_desc
        else:
            image_dict[image] = ""

    template = env.get_template("homepage_template.html")
    homepage = template.render(image_dict=image_dict)

    with open("homepage.html", "w") as outfile:
        outfile.write(homepage)


def convert_image_upload_names_and_copy_files():
    image_upload_list = glob.glob("uploads/*.jpg")
    desc_upload_list = glob.glob("uploads/*.txt")

    pathlib.Path("images/baby").mkdir(parents=True, exist_ok=True)

    for image_file in image_upload_list:
        resize_image(image_file)
        file_create_time = os.path.getctime(image_file)
        shutil.copyfile(image_file, f"images/baby/{file_create_time}--{image_file.replace('uploads/', '')}")

    for desc_file in desc_upload_list:
        shutil.copyfile(desc_file, f"images/baby/{desc_file.replace('uploads/', '')}")


def resize_image(image_file_path):
    with open(image_file_path, "r+b") as f:
        with Image.open(f) as image:
            exif = image.info['exif']
            image.thumbnail([1000, 1000], Image.ANTIALIAS)
            image.save(image_file_path, image.format, exif=exif)


if __name__ == "__main__":
    convert_image_upload_names_and_copy_files()
    create_html()
