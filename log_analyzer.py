#!/usr/bin/env python3

import boto3
import gzip
import shutil
import glob
import os

s3 = boto3.client("s3",
                  aws_access_key_id="AKIA2KF75LS6KAYZHJ5M",
                  aws_secret_access_key="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")

location_dict = {"216.245.221.86": "uptime robot",
                 "72.179.2.157": "Me",
                 "67.189.1.24": "Eugene",
                 "67.189.1.241": "Eugene",
                 "76.184.178.220": "Dallas",
                 "172.56.6.173": "Dallas",
                 "66.68.54.154": "Austin",
                 "72.177.93.18": "Austin",
                 "73.190.106.146": "Beaverton",
                 "107.77.219.122": "Houston",
                 "107.77.221.210": "Houston",
                 "174.222.9.16": "Sacramento",
                 "107.77.219.83": "Houston",
                 "76.99.128.7": "Philadelphia",
                 "47.220.158.26": "Eric Granger (Pflugerville)",
                 "107.77.221.122": "Austin, AT&T Wireless",
                 "66.68.88.88": "Lakeway",
                 "172.58.140.202": "Celeen?"}


def download_log_files():
    for i in range(1, 32):
        if i < 10:
            prefix = f"logs/E24N7IUWPBXJXW.2020-08-0{i}"
        else:
            prefix = f"logs/E24N7IUWPBXJXW.2020-08-{i}"

        log_objects = s3.list_objects_v2(Bucket="aidenoliver.com-logs",
                                         Prefix=prefix)

        if log_objects.get("Contents") is not None:
            for obj in log_objects["Contents"]:
                key = obj["Key"]
                if any([len(glob.glob(key.replace(".gz", ".txt"))) > 0, len(glob.glob(key)) > 0]):
                    print(f"Already downloaded {key}")
                else:
                    if ".gz" in key:
                        print(f"Downloading {key}")
                        s3.download_file(Bucket="aidenoliver.com-logs",
                                         Key=key,
                                         Filename=key)


def unzip_log_files():
    for log_zip in glob.glob("logs/*.gz"):
        print(f"Unzipping {log_zip}")
        with gzip.open(log_zip, "rb") as fin:
            with open(log_zip.replace(".gz", ".txt"), "wb") as fout:
                shutil.copyfileobj(fin, fout)
                print(f"Deleting {log_zip}")
                os.remove(log_zip)


def analyze_log_files():
    access_dict = {}

    for txt_file in glob.glob("logs/*.txt"):
        with open(txt_file) as infile:
            log_content = infile.readlines()

        log_lines = log_content[2:]

        for line in log_lines:
            log_data = line.split("\t")
            ip = log_data[4]
            log_dict = {
                "date": log_data[0],
                "time": log_data[1],
                "ip": ip,
                "status_code": log_data[8],
                "path": log_data[7],
                "user_agent": log_data[10]
            }

            if (current_ip_access_list := access_dict.get(ip)) is None:
                access_dict[ip] = [log_dict]
            else:
                current_ip_access_list.append(log_dict)

    for ip, access_list in access_dict.items():
        if len(access_list) > 10 and ip != "216.245.221.86" and location_dict.get(ip) != "Me":
            print(ip, len(access_list), location_dict.get(ip))
            for thing in access_list:
                print(thing)


download_log_files()
unzip_log_files()
analyze_log_files()
